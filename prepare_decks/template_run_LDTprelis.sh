#!/bin/bash

# Run LDT
#PBS -m ae
#PBS -M ${EMAIL}
#PBS -P ${PROJECT}
#PBS -l walltime=2:20:00
#PBS -l mem=10GB
#PBS -l ncpus=1
#PBS -j oe
#PBS -q normal
#PBS -l wd
#PBS -l storage=gdata/hh5+gdata/${PROJECT}+scratch/${PROJECT}

codepath=${CODEDIR_ROOT}/installs/${EXE_DIR}
if [ -x ${codepath}"/LDT" ]; then
    export PATH="${codepath}:${PATH}"
else
    echo "ERROR: LDT not found"
    exit 1
fi

module purge
module load pbs
module load openmpi/4.0.2
module load netcdf/4.7.1


# Run LIS
LDT ldt.config.prelis

cp ${LIS_DIR}/lis_input* ${BDYDATA_PATH}

